# SynDoc

Ziel dieser Arbeit wird es sein, eine Anforderungsanalyse zu erstellen, um eine prototypische Plattform zu implementieren, der den vorläufigen und den endgültigen Arztbrief gegenüberstellt und darauf hinweist, welche Sätze oder Wörter verändert oder eingefügt worden sind. Diese Synopse wird auch datenschutzrechtlich nach DSGVO geprüft sein. Nach der Gegenüberstellung wird ein „Proof“-Zeichen eingefügt. 

## Allgemeines

Ein Arzt- bzw. Entlassungsbrief stellt ein Kommunikationsmittel zwischen Hausarzt – Klinik und Klinik – Hausarzt da. Außerdem ist der Arztbrief auch ein sektorübergreifender Brief. Zusätzlich hilft das reflektieren des Verlaufs und das Diktieren des Briefes auch, um den Patientenfall noch einmal erneut durchzugehen. Die Arztbriefschreibung hat auch den Zweck der Qualitätssicherung. So ist es Standard, dass der Arztbrief von Fachärzten (bzw. Assistenzärzten) geschrieben wird, vom Oberarzt Korrektur gelesen und danach noch einmal vom Chefarzt der zuständigen Fachabteilung gesehen wird [1]. Um dem Patienten einen Arztbrief nach Entlassung mitzugeben, wird ein vorläufiger Arztbrief ausgehändigt. Ein endgültiger Arztbrief wird dem Hausarzt nachgesendet [2]. Der obige Teil eines Arztbriefes besteht aus den Kontaktdaten der Abteilung, Namen der Chef- und Oberärzte. Ein Arztbrief ist sozusagen auch gleich einer „Visitenkarte“. Zum Inhalt eines Entlassungsbriefes gehört am Anfang der Adressat und die Patientenstammdaten. Die Stammdaten setzen sich aus Namen, Geburtsdatum, Aufnahmenummer, Adresse und Aufenthalt zusammen.  Es folgt die Diagnose, Therapieempfehlung, Epikrise. Zusätzlich sind Informationen enthalten wie z. B. die Medikation bei Aufnahme und die Anamnese, wahlweise noch Allergien oder Befunde. 

*Probleme:*

* Mangelnde Erfahrung im Schreiben von Arztbriefen schon im Studium
* Hohe Fluktuation im ärztlichen Bereich
* Sprachliche Barrieren des ärztlichen Dienstes aus dem Ausland
* Sprachliche Barrieren des Patienten
* Arbeiten mit vorgegebenen Textbausteinen
* Frühe Erstellung welche späteren Entwicklungen nicht mehr fokussieren
* fehlerhafte Spracherkennungssoftware
* Zeitdruck


**Stakeholder:**

1. Primäre Empfänger:
weiterbehandelnder Arzt, namentlich bekannt
2. Sekundäre Empfänger
Ärzte, Rentenversicherungsträger, Rechtsanwälte, Unfallversicherungen, MD, Berufsgenossenschaften, Patienten 

***Arztbrief Beispiel:***

![Arztbrief](ArztbriefPNG.PNG)



